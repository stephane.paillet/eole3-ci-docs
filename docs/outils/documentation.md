# Documentation

MkDocs est l'application qui permet de générer cette documentation.

## Installation MkDocs

Nous installons le paquet `python3-venv` :

```
# apt update && apt install python3-venv --assume-yes
```

Nous créons un virtual env Python et l'activons :

```
python3 -m python3 -m venv ~/.venv/mkdocs
$ . ~/.venv/mkdocs/bin/activate
```

Nous installons mkdocs à l'aide de pip :

```
pip install mkdocs
```

## Récupération sources / construction de la documentation

Pour récupérer les sources de cette documentation :

```
$ git clone git@gitlab.mim-libre.fr:stephane.paillet/eole3-ci-docs.git
```

Pour afficher le contenu en local à l'aide du serveur embarqué :

```
$ mkdocs serve
```

Pour construire la documentation :

```
$ mkdocs build
```

Les fichiers générés se trouvent dans le répertoire `site/`.

```
$ ls site/
404.html  about  addons  css  img  index.html  js  outils  search  search.html  sitemap.xml  sitemap.xml.gz
```

## Aller plus loin

La documentation :

- [getting started](https://www.mkdocs.org/getting-started/)
- [guide utilisateur](https://www.mkdocs.org/user-guide/)