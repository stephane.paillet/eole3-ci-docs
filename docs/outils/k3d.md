# Environnement de test

Au sein du PCLL nous utilisons le cloud privé interne OpenNebula. À adapter en fonction de votre environnement.

## Prérequis

Notre clé SSH doit être intégrée dans [vos paramètres OpenNebula](https://docs.opennebula.io/6.2/management_and_operations/users_groups_management/manage_users.html#manage-your-own-user).

Notre VM `gateway-mensr` doit être démarrée.

## Création de la VM K3d

Nous déployons la VM K3d qui va servir pour nos tests. Choisir le modèle de VM `eolebase-3-k3d-ac-test.fr` et récupérer l’IP de la VM (dans la colonne IPs de l’interface Opennebula). Par défaut c’est l’IP 192.168.0.101.

**NB :** La VM est pré-configurée grâce au script `install-eolebase3.sh` issu du [dépôt GIT provisionner](https://gitlab.mim-libre.fr/EOLE/eole-3/provisionner).

## Paramétrage de la gateway

Il faut maintenant passer sur notre gateway pour régler dnsmasq.

Se connecter en ssh à l’aide de notre clé en activant le transfert d’agent (-A).

```
ssh -A root@192.168.230.ddd
```

Nous adaptons le fichier dnsmasq :

```
cat >> /etc/dnsmasq.d/k3d.conf  <<EOF
address=/.eole3.ac-test.fr/192.168.0.101
EOF
```

Attention, si jamais l’IP du cluster est différente, faire:

```
sed -i "s/101/XXX/" /etc/dnsmasq.d/k3d.conf
```

Redémarrer le service dnsmasq:

```
systemctl restart dnsmasq
```

## Paramétrage de la machine du testeur

Il faut changer l’adresse du DNS pour utiliser celle de votre gateway 192.168.230.ddd
(On peut trouver les adresses de gateway ici )

Ajouter l’adresse du DNS dans le fichier /etc/resolv.conf:

```
nameserver 192.168.230.ddd
```

ou plus directement :

```
sudo sed -i '/search*/a nameserver 192.168.230.ddd' /etc/resolv.conf
```

Ou si vous utilisez systemd :

```
NIC=$(ip -o addr show to 192.168.230.0/24 | awk '{print $2}')
systemd-resolve  --set-dns 192.168.230.ddd --interface $NIC
```

Vérification : le ping doit donner 192.168.0.qqchose (101 la plupart du temps)

```
ping eole3.ac-test.fr
```

## Connexion au cluster k3d

Se connecter root en ssh sur l’adresse IP de la VM eolebase k3d démarrée dans le pas n°1. En principe votre clé ssh est présente sur la VM.

```
ssh root@eole3.ac-test.fr
```

Créer le répertoire de travail :

```
mkdir -p ~/laboite/
cd ~/laboite
```

Vérifier que le cluster k3d répond :

```
kubectl get pods --all-namespaces
```

Le retour de la commande est de ce type :

```
NAMESPACE     NAME                                     READY   STATUS    RESTARTS        AGE
kube-system   coredns-77ccd57875-z8h69                 1/1     Running   1 (4m16s ago)   5m16s
kube-system   local-path-provisioner-957fdf8bc-gk4qr   1/1     Running   1 (4m16s ago)   5m16s
kube-system   metrics-server-648b5df564-7z4s6          1/1     Running   1 (4m16s ago)   5m16s
```

## Réinitialisation du cluster

Pour repartir d'un cluster "propre" lors de vos tests, il est possible de détruire le cluster et de la recréer.

Nous nous plaçons dans le répertoire dédié, et clonons le dépôt du provisionner :

```
cd ~/laboite/
git clone git@gitlab.mim-libre.fr:EOLE/eole-3/provisionner.git && cd provisionner
```

Nous détruisons le cluster actuel :

```
k3d cluster delete eole3
```

Nous réinstallons le cluster

```
bash install-eolebase3.sh
```

Nous pouvons récupérer le fichier kubeconfig.yaml, si nous exécutons les commandes kubectl depuis une autre machine (optionnel) :

```
k3d kubeconfig get eole3 > kubeconfig.yaml
```

## Aller plus loin

- [documentation originale sur le CodiMD](https://codimd.mim-libre.fr/No04nP4eTiCxBO1dXTOxzw#)