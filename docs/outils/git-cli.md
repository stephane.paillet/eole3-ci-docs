# Client GIT

## Installation du client

Pour installer le client GIT :

```
# apt install git
```

## Configuration de l'utilisateur

Afin de pouvoir faire les commit Git avec le bon utilisateur (votre utilisateur GitLab), et non avec le nom de l'utilisateur de l'environnement, nous créons le fichier ~/.gitconfig` contenant les lignes suivantes :

```
[user]
        name = Your name
        email = name@example.com
[alias]
        co    = checkout
        br    = branch
        ci    = commit
        st    = status
        lucid = package lucid
        mb    = merge-base
        sh    = show
        sw    = checkout @{-1}
        lo    = log --oneline
        lu    = log ..@{upstream}
        luo   = log --oneline ..@{upstream}
        ul    = log @{upstream}..HEAD
        ulo   = log --oneline @{upstream}..HEAD
        lg    = log --graph --all --format=format:'%C(bold    blue)%h%C(reset) - %C(white)%s%C(reset) %C(bold white)— %cn%C(reset)%n''          %C(bold cyan)%cD%C(reset) %C(bold green)(%cr)%C(reset) %C(bold yellow)%d%C(reset)' --abbrev-commit
        lg2   = log --graph --all --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%cr)%C(reset) %C(white)%s%C(reset) %C(bold white)— %cn%C(reset)%C(bold yellow)%d%C(reset)' --abbrev-commit --date=relative
	t = tag -s
	id = rev-parse --short
[color]
        interactive = auto
[diff]
        color = auto
	submodule = log
[push]
	default = simple
[pull]
	ff = only
[magit]
	hideCampaign = true
[rerere]
	enabled = true
[init]
	defaultBranch = main
[core]
	editor = vim
	pager = less -r
```

## Aller plus loin

Afin de pouvoir afficher les information sur la branche GIT dans laquelle nous nous trouvons dans le terminal, nous pouvons ajouter ceci dans le fichier `~/.profile` de l'utilisateur :

```
. /etc/bash_completion.d/git-prompt
export GIT_PS1_SHOWDIRTYSTATE=1 GIT_PS1_SHOWUPSTREAM=1
export PS1='($venv_name) \u@\h \w$(__git_ps1 " (%s)")\$ '
```
