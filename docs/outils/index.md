# Outils

Nous utilisons plusieurs outils pour gérer l'intégration :

- un environnement Kubernetes (k3s ou k3d par exemple)
- Eole<sup>3</sup> Tools
- un client GIT
- le [dépôt GIT Eole<sup>3</sup>](https://gitlab.mim-libre.fr/EOLE/eole-3)
- l'instance [Harbor Eole<sup>3</sup>](https://hub.eole.education/)