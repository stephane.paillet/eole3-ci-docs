# Eole3

Eole<sup>3</sup> est la troisième version majeure de la distribution Eole. Elle tient compte de l'évolution des usages numériques ces dernières années, qui s'orientent de plus en plus vers des services en ligne utilisés au travers d'un navigateur.

Cette distribution propose donc des services Web, et les associe à une fédération d'identités et une authentification centralisée unique (SSO), pour offrir une expérience utilisateur unifiée.

La solution utilise la technologie des "conteneurs", et se base sur l'orchestrateur Kubernetes et des images Docker.

La distribution est composée de deux grands ensembles d'applications :

- le "socle", embarquant le frontal de présentation des services et de configuration de l'utilisateurs, de ses groupes... (La Boîte) et les applications comme la gestion d'agendas, le blog, l'application de sondages...
- les "addons", les applications complémentaires (pads, dessin, tableau blanc...)