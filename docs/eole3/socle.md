# Socle

C'est le portail applicatif Eole<sup>3</sup>, la porte d'accès aux différentes applications et services, et les applications installées par défaut. Son URL est du type : https://portail.domain.tld

Le socle embarque les applications suivantes :

| Nom | Description |
| --- | --- |
| Laboite | Portail d'accès aux services Eole<sup>3</sup> |
| Agenda | Agenda partagé |
| Blog | Affichage des articles de blog |
| Blogapi | API REST d'accès aux articles de blog |
| Frontnxt | Aiguilleur d'accès aux nuages Nextcloud |
| Lookup-server | Moteur de recherche des utilisateurs |
| Mezig | Publication des compétences |
| Questionnaire | Création et gestion de questionnaires |
| Radicale | Serveur de calendrier (caldav) |
| Sondage | Création et gestion de sondages |
| Keycloak | Serveur d'authentification Web SSO |
| Mongodb | Base de données en mode replicaset (utilisée par LaBoite) |
| Minio | Serveur de stockage S3 |
| Nginx | Contrôleur ingress par défaut |
| PostgreSQL | Serveur de bases de données (utilisée par Keycloak notamment) |
