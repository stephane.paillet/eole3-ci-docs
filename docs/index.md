# Eole3 CI

**Note importante :** Ce document est une ébauche de documentation pour prendre en main les opérations de CI Eole<sup>3</sup>. Il est en cours de rédaction et n'a pas été validé pour le moment. Merci de ne pas l'utiliser dans l'immédiat.

## Vocabulaire

Afin de clarifier au maximum les termes utilisés dans cette documentation, voici la définition des différents élements.

**Eole<sup>3</sup>**

Eole<sup>3</sup> est la distribution Kubernetes développée par le PCLL, qui propose un ensemble d'applications en ligne cohérent, et un système d'authentification unifié.

**Tools**

Eole<sup>3</sup> Tools est une application chargée de simplifier le déploiement et les mises à jour du socle Eole<sup>3</sup> et des addons Eole<sup>3</sup>.

**Socle**

C'est le portail applicatif Eole<sup>3</sup>, la porte d'accès aux différentes applications et services, et les applications installées par défaut. Son URL est du type : https://portail.domain.tld

**Addons**

Eole<sup>3</sup> Tools permet d'installer un certain nombre d'applications en complément du socle. Ces applications sont présentées sous forme d'addons.

## Gestion de la CI Eole<sup>3</sup>

La gestion de l'Intégration Continue se fait à l'aide de l'instance [GitLab MIM Libre](https://gitlab.mim-libre.fr/EOLE/eole-3).

### Branches

Les dépôts GIT contiennent les branches suivantes :

- dev
- testing
- stable (main ou master)

La branche de travail par défaut est la branche `dev`.

## Disclaimer

Cette documentation n'a pas vocation à remplacer la documentation officielle du [Wiki Eole<sup>3</sup>](https://wiki.eole.education). Le but est de proposer une documentation spécifique pour :

- installer et utiliser Eole<sup>3</sup> Tools dans le cadre de la gestion de l'intégration continue de la distribution
- tester, valider et mettre à jour les conteneurs utilisés par le socle La Boîte et les addons
- tester, valider et mettre à jour les Helm charts utilisés par le socle La Boîte et les addons

Les sources des conteneurs, Helm charts et d'Eole<sup>3</sup> Tools se trouvent au sein des dépôts Git hébergés sur l'instance GitLab MIM Libre :

- [Eole<sup>3</sup> Tools](https://gitlab.mim-libre.fr/EOLE/eole-3/tools)
- [services Eole<sup>3</sup>](https://gitlab.mim-libre.fr/EOLE/eole-3/services)