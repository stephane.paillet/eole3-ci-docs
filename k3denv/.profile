# Copy paste lines below at the end of you .profile file

source ~/laboite/.venv/eole3/bin/activate
eval "$(_EOLE3_COMPLETE=bash_source eole3)"
venv_name=$(basename $VIRTUAL_ENV)
. /etc/bash_completion.d/git-prompt
export GIT_PS1_SHOWDIRTYSTATE=1 GIT_PS1_SHOWUPSTREAM=1
export PS1='($venv_name) \u@\h \w$(__git_ps1 " (%s)")\$ '

