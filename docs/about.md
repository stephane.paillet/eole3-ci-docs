# À propos

EOLE<sup>3</sup> est une solution sous licence libre développée par le [Pôle de Compétence Logiciel Libre](https://pcll.ac-dijon.fr/pcll/) (PCLL). C'est un pôle de compétence du ministère de l’Éducation nationale, spécialisé dans les logiciels libres, leur intégration, leur paramétrage et leur utilisation.

Le PCLL est hebergé au rectorat de Dijon dans la région académique Bourgogne-Franche-Comté.

EOLE est l'acronyme de "**E**nsemble **O**uvert **L**ibre et **E**volutif".

Le PCLL existe depuis le début des années 2000, il a créé les solutions EOLE 1, EOLE 2 (EoleNG). Il édite aujourd'hui la solution EOLE³ (EOLE CUBE en référence à Kubernetes, le socle technique utilisé) et anime la communauté autour des solutions EOLE.

La solution EOLE<sup>3</sup> est déjà utilisée pour déployer et administrer l'offre [Apps Éducation](https://projet.apps.education.fr/) du Ministère de l'Éducation nationale.

## Auteurs

Cette documentation est un travail collégial. Elle est rédigée par :

- Fabrice BARCONNIÈRE
- Joël CUISSINAT
- Daniel DEHENNIN
- Laurent FLORI
- Stéphane PAILLET
- Klaas TJEBBES

## Aller plus loin
Pour parcourir la documentation complète concernant la distribution Eole<sup>3</sup>, vous pouvez suivre le [wiki Eole3](https://wiki.eole.education/).

