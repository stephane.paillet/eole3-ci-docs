# Helm Charts addons : mise à jour

Suivant le type d'addon ("externe" ou "interne"), la mise à jour des Helm Charts des addons sera un peu différente.

## Helm Chart externe (upstream)

Le mise à jour se fait uniquement au niveau de Tools :

- code localisé sur le GitLab MIM-Libre, dans le dépôt [Eole3 Tools](https://gitlab.mim-libre.fr/EOLE/eole-3/tools)

La mise à jour d'une application via un Helm Chart upstream est divisée en deux parties :

- mise à jour et test de l'application en déployant le Helm chart
- mise à jour des fichiers src de l'addon dans Eole Tools

## Helm Chart interne

Dans le cas de la mise à jour d'un Helm Chart interne, il y aura deux niveaux de mise à jour à faire :

- mise à jour du Helm interne
    + code localisé sur le GitLab MIM-Libre, dans le dépôt [Eole3 Services](https://gitlab.mim-libre.fr/EOLE/eole-3/services)
    + Helm Charts déposés sur l'instance [Harbor Eole](https://hub.eole.education) une fois construits par la CI GitLab
- mise à jour de l'addon dans le dépôt Eole<sup>3</sup> Tools
    + code localisé sur le GitLab MIM-Libre, dans le dépôt [Eole3 Tools](https://gitlab.mim-libre.fr/EOLE/eole-3/tools)

La mise à jour d'une application via un Helm Chart interne est divisée en trois parties :

- mise à jour et test de l'application
- mise à jour du Helm chart intégrant la nouvelle version de l'application
- mise à jour des fichiers src de l'addon dans Eole Tools
