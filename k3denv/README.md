# Eole<sup>3</sup> - K3d test environnement

**Note :** Ce répertoire contient quelques informations et fichiers pour déployer et configurer un environnement de test, afin de pouvoir gérer les mises à jour des addons de Eole<sup>3</sup> La Boite.

## Arborescence de fichiers

Le répertoire `laboite` se trouve à la racine du home de l'utilisateur.

```
laboite/
├── *.eole3.ac-test.fr.crt
├── *.eole3.ac-test.fr.key
├── custom-vars
│   └── drawio-vars.yaml
│   └── ...
├── eole3.yaml
├── install
├── provisionner
│   ├── README.md
│   ├── install-eolebase3.sh
│   └── install.sh
├── services
│   └── drawio-helm-chart
│   └── ...
└── tools
```

## Installation

Pour installer un environnement de test `LaBoite` sur la plateforme OpenNebula du PCLL, vous pouvez la suivre procédure équipe EOLE pour k3d laboite se trouvant sur le CodiMD MIM-Libre.
