# Helm Charts

## Helm Charts addons

Chez Eole<sup>3</sup> il y a deux types de Helm Charts :

- les Helm Charts "externes" (tirés de dépôts comme [Artifact Hub](https://artifacthub.io))
- les Helm Charts "internes" (déposés sur l'instance [Arbor Eole](https://hub.eole.education))

## Liste des addons

### CodiMD

- type helm : interne
- helm repo URL : [Harbor Eole](https://hub.eole.education/chartrepo/eole)
- helm src URL :
    - PCLL : [GitLab MIM Libre](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/codimd-helm-chart)
    - upstream : [Github](https://github.com/hackmdio/codimd-helm)

### Collabora

- type helm : interne
- helm repo URL : [Harbor Eole](https://hub.eole.education/chartrepo/eole)
- helm src URL :
    - PCLL : [GitLab MIM Libre](https://gitlab.mim-libre.fr/EOLE/eole-3/services/collabora/collabora-helm-chart/-/blob/dev/helm/)
    - upstream : [Github](https://github.com/CollaboraOnline/online/tree/master/kubernetes/helm/collabora-online)

### Discourse

- type helm : externe
- helm repo URL : [Charts Bitnami](https://charts.bitnami.com/bitnami)
- helm src URL :
    - [Artifact Hub](https://artifacthub.io/packages/helm/bitnami/discourse)
    - [Github](https://github.com/bitnami/charts)

### Drawio

- type helm : interne
- helm repo URL : [Harbor Eole](https://hub.eole.education/chartrepo/eole)
- helm src URL : [GitLab MIM Libre](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart)

### Excalidraw

- type helm : externe
- helm repo URL : [Github Pages](https://johanneskastl.github.io/excalidraw-helm-chart/)
- helm src URL : [Github](https://github.com/johanneskastl/excalidraw-helm-chart)

### FilePizza

- type helm : interne
- helm repo URL : [Harbor Eole](https://hub.eole.education/chartrepo/eole)
- helm src URL : [GitLab MIM Libre](https://gitlab.mim-libre.fr/EOLE/eole-3/services/filepizza/filepizza-helm-chart)

### Mastodon

- type helm : interne
- helm repo URL : [Harbor Eole](https://hub.eole.education/chartrepo/eole)
- helm src URL : [GitLab MIM Libre](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mastodon/mastodon-helm-chart)

### Matomo

- type helm : externe
- helm repo URL : [Bitnami Charts](https://charts.bitnami.com/bitnami)
- helm src URL : [Artifact Hub](https://artifacthub.io/packages/helm/bitnami/matomo)

### Mobilizon

- type helm : interne
- helm repo URL : [Harbor Eole](https://hub.eole.education/chartrepo/eole)
- helm src URL : [GitLab MIM Libre](https://gitlab.mim-libre.fr/EOLE/eole-3/services/mobilizon/mobilizon-helm-chart)

### Nextcloud

- type helm : externe
- helm repo URL : [Nextcloud Pages](https://nextcloud.github.io/helm/)
- helm src URL : [Github](https://github.com/nextcloud/helm)

### Screego

- type helm : interne
- helm repo URL : [Harbor Eole](https://hub.eole.education/chartrepo/eole)
- helm src URL : [GitLab MIM Libre](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart)

### WikiJS

- type helm : externe
- helm repo URL : [WikiJS Charts](https://charts.js.wiki)
- helm src URL : [Github](https://github.com/requarks/wiki/tree/main/dev/helm)
