# Addons

Eole<sup>3</sup> Tools permet d'installer un certain nombre d'applications en complément du socle La Boîte. Ces applications sont présentées sous forme d'addons.

## Liste des addons

| Nom | Description | SSO |
| --- | --- | --- |
| CodiMD | bloc-note collaborative | oui |
| Collabora | Suite collaborative en ligne | non |
| Discourse | forum de discussion | oui (manuel) |
| Drawio | Outil de diagramme en ligne | non |
| Excalidraw | Outil de dessin collaboratif | non |
| Filepizza | Transfert de fichiers P2P | non |
| Mastodon | Réseau social décentralisé | oui |
| Matomo | Serveur de mesure de statistiques web | oui |
| Mobilizon | Gestion d'évènements communautaire | oui |
| Nextcloud | Partage de documents en ligne | oui |
| RocketChat | Serveur de discussion | oui (manuel) |
| Screego | Partage d'écran | non |
| WikiJS | Plateforme de Wiki | oui (manuel) |
