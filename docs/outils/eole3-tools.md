# Eole3 Tools

## Présentation / philosophie

Eole<sup>3</sup> Tools est un outil écrit en Python qui permet d'aider au déploiement et à la mise à jour du socle "La Boîte" Eole<sup>3</sup> (le portail applicatif), et des différentes applications complémentaires (appelées addons).

Il permet de générer les différents fichiers nécessaires au déploiement des Helm charts des applications dans Kubernetes, et de lancer les installations grâce à ces Helm charts. Sans cet outil, il faudrait aller adapter les variables à la main dans chaque Helm chart de chaque application, opération fastidieuse et source d'erreurs.

Afin de générer ces fichiers Helm, il utilise des fichiers de valeurs de type ".ini", et des gabarits Jinja2.

L'utilisation de l'outil se fait de la manière suivante :

- "surcharge" des valeurs par défaut pour correspondre à votre configuration (à l'aide du fichier custom "eole3.yaml")
- génération des fichiers de configuration du portail et des services à déployer avec la commande "eole3 build"
- déploiement de la configuration précédemment générée avec la commande "eole3 deploy"

## Installation

Eole<sup>3</sup> Tools est un module Python. Pour plus de souplesse, nous allons l'installer dans un virtual env Python.

### Installation module Python venv

Nous installons le paquet `python3-venv` :

```
# apt update && apt install python3-venv --assume-yes
```

### Création et activation du virtual env

Nous créons le répertoire `laboite` s'il n'existe pas, et créons le virtual env Python dedans :

```
mkdir ~/laboite
python3 -m venv ~/laboite/.venv/eole3
```

Nous activons le virtual env de la manière suivante :

```
source ~/laboite/.venv/eole3/bin/activate
```

Pour désactiver le virtualenv :

```
deactivate
```

### Installation module Eole<sup>3</sup> Tools

Nous installons le module Python (le virtual env doit être activé). Pour le moment, seule la version dev gère les fichiers yaml en lieu et place des fichiers ini, le paquet final n'étant pas encore publié :

```
pip install git+https://gitlab.mim-libre.fr/EOLE/eole-3/tools.git@dev
```

Quand la version sera publiée en version stable, la commande d'installation sera :

```
pip install --force-reinstall --no-cache-dir  eole3 --index-url https://gitlab.mim-libre.fr/api/v4/projects/494/packages/pypi/simple
```

Pour activer la complétion de la commande `eole3` :

```
eval "$(_EOLE3_COMPLETE=bash_source eole3)"
```

### Activation depuis le fichier profile

Afin de ne pas avoir à activer le virtual env Python et la complétion du module `eole3` à la main à chaque connexion, nous pouvons ajouter ces lignes dans le fichier `.profile` de l'utilisateur :

```
source ~/laboite/.venv/eole3/bin/activate
eval "$(_EOLE3_COMPLETE=bash_source eole3)"
venv_name=$(basename $VIRTUAL_ENV)
```

## Usage

### Aide

Pour afficher l'aide, saisir la commande :

```
eole3 --help
```

Qui devait retourner ceci :

```
Usage: eole3 [OPTIONS] COMMAND [ARGS]...

Options:
  -c, --config PATH  Configuration value file
  --version          Show the version and exit.
  --help             Show this message and exit.

Commands:
  config  Show configuration merged from all the configuration files
  mcm     Generate minimized configuration file with only non-default values
  build   Generate templates
  deploy  Deploy generated templates
  update  Update deployment

```

### Installation / mise à jour du socle

#### Customisation des variables

Nous créons le fichier `eole3.yaml` :

```
cat > /root/laboite/eole3.yaml <<EOF
default:
  #general domain for the deployment
  domain: eole3.ac-test.fr
  smtp:
    protocol: smtp
    hostname: gateway.ac-test.fr

keycloak:
  autoscalingMinReplicas: 2

coredns:
  #Section about internal kubernetes cluster CoreDns server
  patch: true

questionnaire:
  deploy: true
EOF
```

#### Construction des fichiers de déploiement

```
cd ~/laboite/
eole3 --config eole3.yaml build socle
```

#### Ajout des certificats TLS

Nous ajoutons les certificats TLS :

```
cp *.crt ./install/infra/ingress-nginx/tls.crt
cp *.key ./install/infra/ingress-nginx/tls.key
```

#### Déploiement du socle

```
eole3 deploy socle
```

Au bout de quelques minutes après le déploiement complet, un `kubectl get pods -n laboite` devrait donner :

```
kubectl get pods -n laboite
NAME                             READY   STATUS    RESTARTS       AGE
agenda-546495488-cm968           1/1     Running   0              7m33s
blog-6b8b5d59c8-rzmjv            1/1     Running   3 (4m5s ago)   7m33s
blogapi-77fb4b59cd-2ls5n         1/1     Running   0              7m32s
frontnxt-55c7855855-cb4jv        1/1     Running   0              7m33s
keycloak-keycloakx-0             1/1     Running   0              10m
keycloak-keycloakx-1             1/1     Running   0              9m6s
laboite-api-6dd95bbdd8-vmf8m     1/1     Running   0              7m33s
laboite-cdb87896-s97wp           1/1     Running   0              7m33s
lookup-server-6dcfbd659-k5nln    1/1     Running   0              7m33s
mezig-8f5f96b9f-k4wnz            1/1     Running   0              7m33s
minio-0                          1/1     Running   0              11m
minio-1                          1/1     Running   0              11m
minio-2                          1/1     Running   0              11m
minio-3                          1/1     Running   0              11m
mongodb-0                        1/1     Running   0              11m
mongodb-1                        1/1     Running   0              10m
mongodb-2                        1/1     Running   0              10m
mongodb-3                        1/1     Running   0              9m30s
mongodb-4                        1/1     Running   0              8m49s
questionnaire-798f8cc9cb-2n9kv   1/1     Running   0              7m33s
radicale-57b9d6599f-gh29s        1/1     Running   0              7m32s
sondage-7d65f48577-b7q45         1/1     Running   0              7m32s
```

### Installation / mise à jour des addons

#### Customisation des variables

Nous pouvons créer un fichier `custom-vars/drawio-vars.yaml` pour surcharger les variables par défaut.

Par exemple, pour surcharger la version du Helm chart par défaut :

```
drawio:
  chart:
    version: 1.1.0
```

Nous pourrions de la même manière surcharger la version du container utilisé dans le Helm chart :

```
drawio:
  chart:
    imageTag: 22.1.11
```

#### Construction des fichiers de déploiement

```
eole3 --config eole3.yaml build addon -n drawio --config custom-vars/drawio-vars.yaml
```

#### Déploiement de l'addon

```
eole3 deploy addon -n drawio
```

#### Affichage informations

Pour afficher des infos sur le pod :

```
kubectl get pods -n drawio
```

Exemple de retour de la commande :

```
NAME                      READY   STATUS              RESTARTS   AGE
drawio-7c84f9bb65-bhbpn   0/1     ContainerCreating   0          14s
```

Pour lister des informations sur la version du hel et de l'image Docker :

```
helm list -n drawio
```

Exemple de retour de la commande :

```
NAME  	NAMESPACE	REVISION	UPDATED                                	STATUS  	CHART       	APP VERSION
drawio	drawio   	1       	2024-01-17 15:33:07.571000314 +0000 UTC	deployed	drawio-1.1.0	22.1.11   
```

#### Désinstallation de l'addon

```
helm uninstall -n drawio drawio
```

### Information de configuration

Pour récupérer les variables de configuration du socle :

```
eole3 config socle
```

Pour récupérer les variables de configuration d'un addon :

```
eole3 config -n drawio
```

## Aller plus loin

Vous trouverez des informations complémentaires sur le dépôt GitLab :

- [dépôt Git Eole<sup>3</sup> Tools](https://gitlab.mim-libre.fr/EOLE/eole-3/tools)
- [getting started](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/blob/dev/GETTING_STARTED.md)