# Mise à jour addon : Helm Chart interne

Nous allons prendre comme exemple la mise à jour de l'application Screego.

Nous vérifions les versions de l'application et du Helm Chart installés au sein de notre dépôt GitLab du service Screego.

## Mise à jour de l'application

### Récupération de la version de l'application

Nous vérifions la version de l'application sur le dépôt GitLab en local (ici git-eole3/charts/addons/screego/screego-helm-chart/helm/Chart.yaml). La vérification peut aussi se faire depuis le [dépôt GitLab du service](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/-/blob/dev/helm/Chart.yaml)

```  
apiVersion: v2
name: screego
description: A Helm chart for Kubernetes
type: application
version: 1.0.0-dev.5
appVersion: 1.10.0
```

Nous recherchons le dépôt du conteneur utilisé afin d'aller chercher le numéro de la version la plus récente. Cette information est disponible dans le fichier [values.yaml](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/-/blob/dev/helm/values.yaml?ref_type=heads) du Helm Chart

```  
image:
  repository: screego/server
```  

Nous vérifions quelle version est disponible sur le Docker Hub à l'aide de son tag. Ici la dernière version est la v1.10.2

![docker registry](../img/addon_upgrade01.png)

### Test de la nouvelle version

Nous créons un fichier screego-values.yml

``` 
[screego]
tag=1.10.2
``` 

Nous construisons les fichiers de déploiement à partir de ce fichier

``` 
eole3 --config socle.ini build addon -n screego --config screego-values.yml
``` 

Nous déployons l'addon

``` 
eole3 deploy addon -n screego
``` 

Nous relevons le nom du pod

``` 
get pod -n screego
``` 

Nous vérifions la version de l'application installée

``` 
kubectl describe pod -n screego screego-7c84f9bb65-45ffw
``` 

### Création de l'espace de travail

Nous nous rendons sur le dépôt GitLab de l'[application Screego](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart), et créons un ticket pour demander la mise à jour de l'application en 1.10.2

![new issue to upgrade app](../img/addon_upgrade02.png)

Nous créons une merge request, qui va créer le dépôt dédié à cette opération

![new merge request to upgrade app](../img/addon_upgrade03.png)

Nous listons à l'aide de la commande `git fetch` la nouvelle branche créée automatiquement par le workflow GitLab, et nous plaçons dedans à l'aide de la commande `git checkout` (ou "co" ici car nous utilisons un alias)

![merge repo checkout](../img/addon_upgrade04.png)

Nous éditons le fichier pour mettre à jour le paramètre `appVersion` à la dernière version de l'application dans le fichier `helm/Chart.yaml`

``` 
apiVersion: v2
name: screego
description: A Helm chart for Kubernetes
type: application
version: 1.0.0-dev.5
appVersion: 1.10.2
``` 

### Test de l'application

Le test de l'application se fait de deux manières :
- installation from scratch
- mise à jour depuis la version inférieure

Pour créer les fichiers de déploiement

``` 
eole3 --config socle.ini build addon -n screego
``` 

Pour déployer depuis les fichiers précédemment créés

``` 
eole3 deploy addon -n screego
``` 

Pour vérifier les versions du helm et de l'application installés

``` 
helm list -n screego
``` 

### Publication de la nouvelle version de l'application

``` 
git add helm/Chart.yaml
git commit -m "feat: update appversion to 1.10.2"
git push
``` 

![add and push new Chart file](../img/addon_upgrade05.png)

Dans l'interface Web de GitLab, nous cliquons sur le bouton "Mak as ready"

![mark as ready merge request](../img/addon_upgrade06.png)

Et enfin nous cliquons sur le bouton "Merge"

![merge changes](../img/addon_upgrade07.png)

Nous nous plaçons sur la branche dev locale, et mettons à jour la branche

![pull merged version to dev](../img/addon_upgrade08.png)

La CI a incrémenté la version du Helm Chart. Le fichier `helm/Chart.yaml` contient maintenant une nouvelle version de chart

``` 
apiVersion: v2
name: screego
description: A Helm chart for Kubernetes
type: application
version: 1.0.0-dev.6
appVersion: 1.10.2
``` 

### Publication dans la branche testing

Nous passons maintenant cette version du dépôt dev dans le dépôt testing. Nous créons un nouveau ticket

![new issue dev to testing](../img/addon_upgrade09.png)

Nous choisissons dans le bouton "Create merge request" la branche testing

![merge request creation dev to testing](../img/addon_upgrade10.png)

Dans la console, nous listons la nouvelle branche avec la commande `git fetch` et nous positionnons dedans avec la commande `git co`

![checkout dev to testing](../img/addon_upgrade11.png)

Nous mergeons dans la branche testing

![merge dev to testing](../img/addon_upgrade12.png)

Dans la merge request, nous cliquons sur le bouton "Mark as ready"

![mark as ready dev to testing](../img/addon_upgrade13.png)

Puis sur le bouton "Merge"

![merge dev to testing](../img/addon_upgrade14.png)

Dans la console, nous nous plaçons dans la branche testing, et mettons à jour depuis le dépôt distant

![pull in testing](../img/addon_upgrade15.png)

### Publication dans la branche stable

Nous créons un ticket pour demander la publication sur la branche stable

![create issue testing to main](../img/addon_upgrade16.png)

Nous cliquons sur le bouton de création de Merge request en choisissant la branche "main"

![create merge request from testing to main](../img/addon_upgrade17.png)

Nous confirmons la création de la merge request

![confirm merge request dev to testing](../img/addon_upgrade18.png)

Nous listons les nouvelles branches, nous plaçons dans la branche de la merge request, et mergeons depuis testing

![merge testing to main](../img/addon_upgrade19.png)

Nous poussons le contenu mergé dans la branche

![push testing to main](../img/addon_upgrade20.png)

Nous retournons dans la merge request GitLab, et cliquons sur le bouton "Mark as ready"

![push testing to main](../img/addon_upgrade21.png)

Nous cliquons sur le bouton "Merge"

![push testing to main](../img/addon_upgrade22.png)

Nous nous plaçons dans la branche main et mettons à jour à partir du dépôt distant

![push testing to main](../img/addon_upgrade23.png)

## Mise à jour du Helm Chart

Nous créons un ticket pour mettre à jour le Helm Chart Screego en version 1.0.0

![new issue to upgrade helm chart](../img/addon_upgrade24.png)

Nous cliquons sur le bouton "create merge request"

![create merge request to upgrade helm chart](../img/addon_upgrade25.png)

Nous confirmons l'opération "create merge request"

![confirm merge request to upgrade helm chart](../img/addon_upgrade26.png)

